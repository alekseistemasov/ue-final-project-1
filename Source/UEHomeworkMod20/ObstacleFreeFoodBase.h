// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FoodBase.h"
#include "ObstacleFreeFoodBase.generated.h"

/**
 * 
 */
UCLASS()
class UEHOMEWORKMOD20_API AObstacleFreeFoodBase : public AFoodBase
{
	GENERATED_BODY()


protected:
	UPROPERTY(EditDefaultsOnly)
	float ActiveTime;

public:
	virtual void Interact(AActor* interactor, bool bIsHead) override;
	
};
