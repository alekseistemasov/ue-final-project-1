// Fill out your copyright notice in the Description page of Project Settings.


#include "ObstacleFreeFoodBase.h"
#include "SnakeActorBase.h"

void AObstacleFreeFoodBase::Interact(AActor* interactor, bool bIsHead)
{
	ASnakeActorBase* snake = Cast<ASnakeActorBase>(interactor);
	if (IsValid(snake)) {
		snake->ActivateObstacleFreeStatus(ActiveTime);
	}
	Super::Interact(interactor, bIsHead);
}
