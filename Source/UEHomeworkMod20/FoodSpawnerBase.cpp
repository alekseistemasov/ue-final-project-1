// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodSpawnerBase.h"
#include "Math/UnrealMathUtility.h"
#include "FoodBase.h"
#include "SpeedUpFoodBase.h"
#include "SlowDownFoodBase.h"
#include "ObstacleFreeFoodBase.h"
#include "DoubleScoreFoodBase.h"
#include "GridFieldBase.h"


// Sets default values
AFoodSpawnerBase::AFoodSpawnerBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	

}

// Called when the game starts or when spawned
void AFoodSpawnerBase::BeginPlay()
{
	Super::BeginPlay();
	
}





// Called every frame
void AFoodSpawnerBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFoodSpawnerBase::SpawnFood()
{
	if (!IsValid(Grid)) return;
	int random = FMath::RandRange(0, 10);
	FGridElement gridElement = Grid->TakeOverRandom();
	AFoodBase* food = nullptr;
	
	switch (random) {
	case 0:
	case 1:
	case 2:
	case 3:
	case 4:
	case 5:
	case 6: {
		food = GetWorld()->SpawnActor<AFoodBase>(FoodClass, FTransform(gridElement.Position));
		break;
	}
	case 7: {
		food = GetWorld()->SpawnActor<ASpeedUpFoodBase>(SpeedUpFoodClass, FTransform(gridElement.Position));
		break;
	}
	case 8: {
		food = GetWorld()->SpawnActor<ASlowDownFoodBase>(SlowDownFoodClass, FTransform(gridElement.Position));
		break;
	}
	case 9: {
		food = GetWorld()->SpawnActor<AObstacleFreeFoodBase>(ObstacleFreeFoodClass, FTransform(gridElement.Position));
		break;
	}
	case 10: {
		food = GetWorld()->SpawnActor<ADoubleScoreFoodBase>(DoubleScoreFoodClass, FTransform(gridElement.Position));
		break;
	}
	}


    if (IsValid(food)) {
	    food->Spawner = this;
		food->GridElementIndex = gridElement.Index;
	}
	

}

void AFoodSpawnerBase::OnFoodDestroyed(int gridElementIndex)
{
	Grid->ReleaseByIndex(gridElementIndex);
	SpawnFood();
}

void AFoodSpawnerBase::Init(AGridFieldBase* grid)
{
	Grid = grid;
	SpawnFood();
}

