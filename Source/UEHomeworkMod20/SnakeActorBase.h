// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeActorBase.generated.h"


class ASnakeElementBase;
class AGridFieldBase;
class APlayerPawnBase;

UENUM()
enum class EMovementDirection : uint8 {
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class UEHOMEWORKMOD20_API ASnakeActorBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeActorBase();

	UPROPERTY()
	bool bCanMove;

	

	

	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeHeadClass;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElementsArr;

	UPROPERTY()
	AGridFieldBase* Grid;

	UPROPERTY()
	APlayerPawnBase* OwnerPawn;

	UPROPERTY(EditDefaultsOnly)
	float Speed;

	UPROPERTY()
	bool bCanAcceptInput;

	UPROPERTY()
	EMovementDirection LastDirection;

	UPROPERTY()
	float LifeTimer;

	UPROPERTY(EditDefaultsOnly)
	float StartLifeTime;

	UPROPERTY()
	int Score;

	

	UPROPERTY()
	bool bIsObstacleFree;

	UPROPERTY()
	float ObstacleFreeTimer;

	UPROPERTY()
	bool bIsDoubleScore;

	UPROPERTY()
	float DoubleScoreTimer;

	UPROPERTY(EditDefaultsOnly)
	UMaterial* ObstacleFreeMaterial;

	UPROPERTY(EditDefaultsOnly)
	UMaterial* DoubleScoreMaterial;

	UPROPERTY(EditDefaultsOnly)
	UMaterial* StandardMaterial;

	void Move();

	void DeactivateObstacleFreeStatus();

	void DeactivateDoubleScoreStatus();

	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddSnakeElements(int elementsNum = 1, bool isFirstInit = false);

	void SnakeElementOverlap(ASnakeElementBase* OverlapedBlock, AActor* Other);

	void DestroySnake();

	void SetDirection(EMovementDirection Direction);

	EMovementDirection GetDirection();

	void AddLifeTime(float time);

	float GetLifeTime();

	float GetStartLifeTime();

	void AddScore(int value);

	int GetScore();

	void Init(AGridFieldBase* grid, APlayerPawnBase* pawn);

	int GetHeadGridIndex();

	void ActivateObstacleFreeStatus(float time);

	bool GetObstacleFreeStatus();

	void ActivateDoubleScoreStatus(float time);

	bool GetDoubleScoreStatus();

	

	

};
