// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeGameModeBase.h"
#include "GridFieldBase.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerPawnBase.h"
#include "FoodSpawnerBase.h"
#include "ObstacleSpawnerBase.h"

void ASnakeGameModeBase::StartPlay()
{
	Super::StartPlay();
	Grid = GetWorld()->SpawnActor<AGridFieldBase>(GridClass, FTransform(GridPosition));
	APlayerPawnBase* pawn = Cast<APlayerPawnBase>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (IsValid(pawn)) {
		pawn->Init(Grid, this);
	}
	


}



void ASnakeGameModeBase::SnakeStarted_Implementation()
{
	APlayerPawnBase* pawn = Cast<APlayerPawnBase>(UGameplayStatics::GetPlayerPawn(this, 0));
	FoodSpawner = GetWorld()->SpawnActor<AFoodSpawnerBase>(FoodSpawnerClass);
	FoodSpawner->Init(Grid);
	ObstacleSpawner = GetWorld()->SpawnActor<AObstacleSpawnerBase>(ObstacleSpawnerClass);
	ObstacleSpawner->Init(Grid, pawn);
}

void ASnakeGameModeBase::SnakeDestroyed_Implementation()
{
	if (IsValid(FoodSpawner)) FoodSpawner->Destroy();
	if (IsValid(ObstacleSpawner)) ObstacleSpawner->Destroy();
}
