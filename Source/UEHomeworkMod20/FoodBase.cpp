// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodBase.h"
#include "SnakeActorBase.h"
#include "FoodSpawnerBase.h"
#include "Components/WidgetComponent.h"
#include "LifeTimeWidgetBase.h"
#include "Components/StaticMeshComponent.h"


// Sets default values
AFoodBase::AFoodBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Points = 10;
	TimeToAdd = 5;
	MinLifeTime = 3;
	MaxLifeTime = 15;
	
	
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComp"));
	LifeTimeWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("LifeTime"));
	LifeTimeWidget->SetupAttachment(RootComponent);
	

}

// Called when the game starts or when spawned
void AFoodBase::BeginPlay()
{
	Super::BeginPlay();
	ULifeTimeWidgetBase* widget = Cast<ULifeTimeWidgetBase>(LifeTimeWidget->GetWidget());
	if (IsValid(widget)) {
		widget->Food = this;
	}
	StartLifeTime = FMath::RandRange(MinLifeTime, MaxLifeTime);
	LifeTimer = StartLifeTime;
	
	
}

// Called every frame
void AFoodBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	LifeTimer -= DeltaTime;

	if (LifeTimer <= 0) {
		if (IsValid(Spawner)) Spawner->OnFoodDestroyed(GridElementIndex);
		this->Destroy();
	}

}

float AFoodBase::GetLifeTimePercent()
{
	return LifeTimer / StartLifeTime;
}

void AFoodBase::Interact(AActor* interactor, bool bIsHead)
{
	ASnakeActorBase* snake = Cast<ASnakeActorBase>(interactor);
	if (IsValid(snake)) {
		snake->AddSnakeElements();
		int pointsToAdd = 0;
		if (snake->GetDoubleScoreStatus()) pointsToAdd = Points * 2;
		else pointsToAdd = Points;
		snake->AddScore(pointsToAdd);
		snake->AddLifeTime(TimeToAdd);
		if (IsValid(Spawner)) Spawner->OnFoodDestroyed(GridElementIndex);
		this->Destroy();
	}

}

