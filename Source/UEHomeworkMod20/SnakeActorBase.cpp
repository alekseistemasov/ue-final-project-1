// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeActorBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "GridFieldBase.h"
#include "PlayerPawnBase.h"

// Sets default values
ASnakeActorBase::ASnakeActorBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Speed = 1;
	LastDirection = EMovementDirection::DOWN;
	bCanAcceptInput = true;
	Score = 0;
	StartLifeTime = 30;
	bCanMove = false;
	bIsObstacleFree = false;
	bIsDoubleScore = false;

}

// Called when the game starts or when spawned
void ASnakeActorBase::BeginPlay()
{
	Super::BeginPlay();
	LifeTimer = StartLifeTime;
	SetActorTickInterval(Speed);
	
}

// Called every frame
void ASnakeActorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (bCanMove) {
		Move();
		bCanAcceptInput = true;
		LifeTimer -= DeltaTime;
	}

	if (LifeTimer <= 0) {
		DestroySnake();
	}
	
	if (ObstacleFreeTimer > 0) {
		ObstacleFreeTimer -= DeltaTime;
	}
	else {
		if (bIsObstacleFree) DeactivateObstacleFreeStatus();
	}

	if (DoubleScoreTimer > 0) {
		DoubleScoreTimer -= DeltaTime;
	}
	else {
		if (bIsDoubleScore) DeactivateDoubleScoreStatus();
	}


}

void ASnakeActorBase::AddSnakeElements(int elementsNum, bool isFirstInit)
{
	if (!IsValid(Grid)) return;
	
	for (int i = 0; i < elementsNum; i++) {
		ASnakeElementBase* element = nullptr;
		if (i == 0 && isFirstInit) element = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeHeadClass);
		else element = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass);
		int index = SnakeElementsArr.Add(element);
		FGridElement gridElement;

		if (index == 0) {
			gridElement = Grid->TakeOverMiddle();
			element->SetFirstElementType();
		}
		else {
			if (isFirstInit) {
				gridElement = Grid->TakeOver(SnakeElementsArr[index - 1]->GridElementIndex, EMovementDirection::UP);
			}
			else {
				gridElement = Grid->TakeOverByIndex(SnakeElementsArr[index - 1]->GridElementIndex, SnakeElementsArr[index - 1]->GridElementIndex);
			}
		}
		element->GridElementIndex = gridElement.Index;
		element->SetActorLocation(gridElement.Position);
		if (!isFirstInit) element->Mesh->SetVisibility(false);
		if (bIsObstacleFree) element->Mesh->SetMaterial(0,ObstacleFreeMaterial);
		if (bIsDoubleScore) element->Mesh->SetMaterial(0, DoubleScoreMaterial);
		element->SnakeOwner = this;
		
		
	}
}

void ASnakeActorBase::Move()
{
	if (!IsValid(Grid)) return;

	int prevElementGridIndex = SnakeElementsArr[0]->GridElementIndex;
	FGridElement newHeadGridElement = Grid->TakeOver(SnakeElementsArr[0]->GridElementIndex, LastDirection);
	SnakeElementsArr[0]->GridElementIndex = newHeadGridElement.Index;
	SnakeElementsArr[0]->SetActorLocation(newHeadGridElement.Position);

	switch (LastDirection) {
	case EMovementDirection::RIGHT:
		SnakeElementsArr[0]->SetActorRotation(FQuat::MakeFromEuler(FVector(0, 0, -90)));
		break;
	case EMovementDirection::DOWN:
		SnakeElementsArr[0]->SetActorRotation(FQuat::MakeFromEuler(FVector(0, 0, 0)));
		break;
	case EMovementDirection::LEFT:
		SnakeElementsArr[0]->SetActorRotation(FQuat::MakeFromEuler(FVector(0, 0, 90)));
		break;
	case EMovementDirection::UP:
		SnakeElementsArr[0]->SetActorRotation(FQuat::MakeFromEuler(FVector(0, 0, 180)));
		break;
	default:
		break;
	}
	
	SnakeElementsArr[0]->Mesh->SetVisibility(true);
	
	for (int i = 1; i < SnakeElementsArr.Num(); i++) {
		auto currentElement = SnakeElementsArr[i];
		FGridElement newGridElement = Grid->TakeOverByIndex(currentElement->GridElementIndex, prevElementGridIndex);
		prevElementGridIndex = currentElement->GridElementIndex;
		currentElement->GridElementIndex = newGridElement.Index;
		currentElement->SetActorLocation(newGridElement.Position);
		currentElement->Mesh->SetVisibility(true);
    }

	
}

void ASnakeActorBase::DeactivateObstacleFreeStatus()
{
	bIsObstacleFree = false;
	for (auto& element : SnakeElementsArr) {
		element->Mesh->SetMaterial(0, StandardMaterial);
	}
}

void ASnakeActorBase::DeactivateDoubleScoreStatus()
{
	bIsDoubleScore = false;
	for (auto& element : SnakeElementsArr) {
		element->Mesh->SetMaterial(0, StandardMaterial);
	}
}

void ASnakeActorBase::SnakeElementOverlap(ASnakeElementBase* OverlapedBlock, AActor* Other)
{
	int index;
	SnakeElementsArr.Find(OverlapedBlock, index);
	bool bIsFirst = index == 0;
	IInteractable* interactableInterface = Cast<IInteractable>(Other);
	
	if (interactableInterface) {
		interactableInterface->Interact(this, bIsFirst);
	}
}

void ASnakeActorBase::DestroySnake()
{
	for (auto& element : SnakeElementsArr) {
		element->Destroy();

	}
	if (IsValid(OwnerPawn)) OwnerPawn->OnSnakeDestroyed();
	this->Destroy();
}

void ASnakeActorBase::SetDirection(EMovementDirection Direction)
{
	if (!bCanAcceptInput) return;
	LastDirection = Direction;
	bCanAcceptInput = false;
}

EMovementDirection ASnakeActorBase::GetDirection()
{
	return LastDirection;
}

void ASnakeActorBase::AddLifeTime(float time)
{
	LifeTimer += time;
	if (LifeTimer > StartLifeTime) LifeTimer = StartLifeTime;
}

float ASnakeActorBase::GetLifeTime()
{
	return LifeTimer;
}

float ASnakeActorBase::GetStartLifeTime()
{
	return StartLifeTime;
}

void ASnakeActorBase::AddScore(int value)
{
	Score += value;
	
}

int ASnakeActorBase::GetScore()
{
	return Score;
}



void ASnakeActorBase::Init(AGridFieldBase* grid, APlayerPawnBase* pawn)
{
	Grid = grid;
	OwnerPawn = pawn;
	AddSnakeElements(4, true);
}

int ASnakeActorBase::GetHeadGridIndex()
{
	return SnakeElementsArr[0]->GridElementIndex;
}

void ASnakeActorBase::ActivateObstacleFreeStatus(float time)
{
	if (bIsDoubleScore) DeactivateDoubleScoreStatus();
	bIsObstacleFree = true;
	ObstacleFreeTimer = time;
	for (auto& element : SnakeElementsArr) {
		element->Mesh->SetMaterial(0, ObstacleFreeMaterial);
	}
}

bool ASnakeActorBase::GetObstacleFreeStatus()
{
	return bIsObstacleFree;
}

void ASnakeActorBase::ActivateDoubleScoreStatus(float time)
{
	if (bIsObstacleFree) DeactivateObstacleFreeStatus();
	bIsDoubleScore = true;
	DoubleScoreTimer = time;
	for (auto& element : SnakeElementsArr) {
		element->Mesh->SetMaterial(0, DoubleScoreMaterial);
	}
}

bool ASnakeActorBase::GetDoubleScoreStatus()
{
	return bIsDoubleScore;
}





