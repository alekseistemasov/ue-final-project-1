// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GridFieldBase.generated.h"


enum class EMovementDirection : uint8;
class AWallBase;

USTRUCT()
struct FGridElement {
	GENERATED_BODY()
	
	FVector Position;
	bool bIsEmpty;
	int Index;
};

UCLASS()
class UEHOMEWORKMOD20_API AGridFieldBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGridFieldBase();

	

	



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void CreateGrid();

	TArray<FGridElement> GridElements;

	UPROPERTY(EditDefaultsOnly)
	int GridSize;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AWallBase> WallClass;

	void CreateWall();

	FGridElement GetElement(int lastElementIndex, EMovementDirection direction);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	FGridElement TakeOver(int lastElementIndex, EMovementDirection direction, bool bReleaseLastIndex = true);

	FGridElement TakeOverByIndex(int lastElementIndex, int currentIndex);

	FGridElement TakeOverMiddle();

	FGridElement TakeOverRandom();

	void ReleaseByIndex(int index);

	FGridElement GetInfo(int lastElementIndex, EMovementDirection direction);

};
