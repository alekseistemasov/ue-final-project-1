// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElementBase.h"
#include "Components/StaticMeshComponent.h"
#include "SnakeActorBase.h"


// Sets default values
ASnakeElementBase::ASnakeElementBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("MainComp"));
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	Mesh->SetupAttachment(RootComponent);
	
	if (IsValid(Mesh)) {
		Mesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
		Mesh->SetCollisionResponseToAllChannels(ECR_Overlap);
	}
	
	

}

// Called when the game starts or when spawned
void ASnakeElementBase::BeginPlay()
{
	Super::BeginPlay();
	
	
}

// Called every frame
void ASnakeElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeElementBase::SetFirstElementType_Implementation()
{
	
	Mesh->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);
}


void ASnakeElementBase::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent, 
	AActor* Other, 
	UPrimitiveComponent* OtherComponent, 
	int32 OtherBodyIndex, 
	bool bFromSweep, 
	const FHitResult& SweepResult)
{
	
	if (IsValid(SnakeOwner)) {
		SnakeOwner->SnakeElementOverlap(this, Other);
	}
}

void ASnakeElementBase::ToogleCollision()
{
	if (Mesh->GetCollisionEnabled() == ECollisionEnabled::NoCollision) {
		Mesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	else {
		Mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}

void ASnakeElementBase::Interact(AActor* interactor, bool bIsHead)
{
	ASnakeActorBase* snake = Cast<ASnakeActorBase>(interactor);
	if (IsValid(snake)) {
		snake->DestroySnake();
	}

}

