// Fill out your copyright notice in the Description page of Project Settings.


#include "DoubleScoreFoodBase.h"
#include "SnakeActorBase.h"

void ADoubleScoreFoodBase::Interact(AActor* interactor, bool bIsHead)
{
	ASnakeActorBase* snake = Cast<ASnakeActorBase>(interactor);
	if (IsValid(snake)) snake->ActivateDoubleScoreStatus(DoubleScoreTime);
	Super::Interact(interactor, bIsHead);

}
