// Fill out your copyright notice in the Description page of Project Settings.


#include "ObstacleSpawnerBase.h"
#include "GridFieldBase.h"
#include "SnakeActorBase.h"
#include "ObstacleBase.h"
#include "PlayerPawnBase.h"

// Sets default values
AObstacleSpawnerBase::AObstacleSpawnerBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	MinObstacleBlocks = 3;
	MaxObstacleBlocks = 7;
	MinObstacleLifeTime = 3;
	MaxObstacleLifeTime = 25;

}

// Called when the game starts or when spawned
void AObstacleSpawnerBase::BeginPlay()
{
	Super::BeginPlay();
	
}

void AObstacleSpawnerBase::SpawnObstacle()
{
	if (!IsValid(Grid) || !IsValid(Pawn)) return;
	
	TArray<int> indexes;
	int SnakeHeadGridIndex = Pawn->GetSnakeHeadGridIndex();
	for (int i = 0; i < 4; i++) {
		
		EMovementDirection direction = EMovementDirection::DOWN;
		switch (i) {
		case 0:
			direction = EMovementDirection::UP;
			break;
		case 1:
			direction = EMovementDirection::RIGHT;
			break;
		case 2:
			direction = EMovementDirection::DOWN;
			break;
		case 3:
			direction = EMovementDirection::LEFT;
			break;

		}
		if (Grid->GetInfo(SnakeHeadGridIndex, direction).bIsEmpty) {
			int gridIndex = Grid->TakeOver(SnakeHeadGridIndex, direction, false).Index;
			indexes.Add(gridIndex);
			if (Grid->GetInfo(gridIndex, direction).bIsEmpty) gridIndex = Grid->TakeOver(gridIndex, direction, false).Index;
			indexes.Add(gridIndex);
		}

	}
	
    int random = FMath::RandRange(MinObstacleBlocks, MaxObstacleBlocks);
	int lastObstacleIndex = 0;
	float randomLifeTime = FMath::RandRange(MinObstacleLifeTime, MaxObstacleLifeTime);
	for (int i = 0; i < random; i++) {
		FGridElement gridElement;
		EMovementDirection direction = EMovementDirection::DOWN;
		int randomDirection = FMath::RandRange(0, 3);
		switch (randomDirection) {
		case 0:
			direction = EMovementDirection::UP;
			break;
		case 1:
			direction = EMovementDirection::RIGHT;
			break;
		case 2:
			direction = EMovementDirection::DOWN;
			break;
		case 3:
			direction = EMovementDirection::LEFT;
			break;
		}
		
		if (i == 0) {
			gridElement = Grid->TakeOverRandom();
		}
		else {
			if (!Grid->GetInfo(lastObstacleIndex, direction).bIsEmpty) continue;
			gridElement = Grid->TakeOver(lastObstacleIndex, direction, false);
			
		}
		AObstacleBase* obstacle = GetWorld()->SpawnActor<AObstacleBase>(ObstacleClass, FTransform(gridElement.Position));
		obstacle->GridElementIndex = gridElement.Index;
		obstacle->Spawner = this;
		obstacle->Init(randomLifeTime);
		Obstacles.Add(obstacle);
		lastObstacleIndex = gridElement.Index;



	}

	for (auto i : indexes) {
		Grid->ReleaseByIndex(i);
	}

}

// Called every frame
void AObstacleSpawnerBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AObstacleSpawnerBase::Init(AGridFieldBase* grid, APlayerPawnBase* pawn)
{
	Grid = grid;
	Pawn = pawn;
	SpawnObstacle();
}

void AObstacleSpawnerBase::OnObstacleDestroyed(int gridElementIndex)
{
	Grid->ReleaseByIndex(gridElementIndex);
	Obstacles.Pop();
	if (Obstacles.IsEmpty()) SpawnObstacle();
}

