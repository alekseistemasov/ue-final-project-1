// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Camera/CameraComponent.h"
#include "SnakeActorBase.h"
#include "GridFieldBase.h"
#include "SnakeGameModeBase.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	RootComponent = Camera;

}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	

	
	
	

	
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandleVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandleHorizontalInput);
	PlayerInputComponent->BindAction("SpacePushed", EInputEvent::IE_Pressed, this, &APlayerPawnBase::HandleSpacePressed);
	

}

void APlayerPawnBase::CreateSnakeActor(AGridFieldBase* grid)
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeActorBase>(SnakeActorClass, FTransform(FVector(0, 0, 60)));
	SnakeActor->Init(grid, this);
}

void APlayerPawnBase::HandleVerticalInput(float value)
{
	if (IsValid(SnakeActor)) {
		
		
		if (value > 0 && SnakeActor->GetDirection() != EMovementDirection::DOWN) {
			SnakeActor->SetDirection(EMovementDirection::UP);
			
		}
		else if (value < 0 && SnakeActor->GetDirection() != EMovementDirection::UP) {
			SnakeActor->SetDirection(EMovementDirection::DOWN);
			
		}
	}
}

void APlayerPawnBase::HandleHorizontalInput(float value)
{
	if (IsValid(SnakeActor)) {
		
		
		if (value > 0 && SnakeActor->GetDirection() != EMovementDirection::LEFT) {
			SnakeActor->SetDirection(EMovementDirection::RIGHT);
		}
		else if (value < 0 && SnakeActor->GetDirection() != EMovementDirection::RIGHT) {
			SnakeActor->SetDirection(EMovementDirection::LEFT);
		}
	}
}

void APlayerPawnBase::HandleSpacePressed()
{
	if (IsValid(SnakeActor)) SnakeActor->bCanMove = true;
	else {
		CreateSnakeActor(Grid);
		if (IsValid(SnakeActor)) SnakeActor->bCanMove = true;
	}
	GameMode->SnakeStarted();
}

int APlayerPawnBase::GetScore()
{
	if (!IsValid(SnakeActor)) return -1;
	else return SnakeActor->GetScore();
}

float APlayerPawnBase::GetLifeTimePercent()
{
	if (!IsValid(SnakeActor)) return 0;
	else return SnakeActor->GetLifeTime() / SnakeActor->GetStartLifeTime();
}



void APlayerPawnBase::Init(AGridFieldBase* grid, ASnakeGameModeBase* gameMode)
{
	Grid = grid;
	GameMode = gameMode;
	CreateSnakeActor(grid);

}

int APlayerPawnBase::GetSnakeHeadGridIndex()
{
	return SnakeActor->GetHeadGridIndex();
}

void APlayerPawnBase::OnSnakeDestroyed()
{
	if (IsValid(GameMode)) GameMode->SnakeDestroyed();
}

