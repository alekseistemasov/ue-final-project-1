// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "FoodBase.h"
#include "SpeedUpFoodBase.generated.h"

class AFoodSpawnerBase;

UCLASS()
class UEHOMEWORKMOD20_API ASpeedUpFoodBase : public AFoodBase
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpeedUpFoodBase();

	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
    
	UPROPERTY(EditDefaultsOnly)
	float SpeedToAdd;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* interactor, bool bIsHead) override;

};
