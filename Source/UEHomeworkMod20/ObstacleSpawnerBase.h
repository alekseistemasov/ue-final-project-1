// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ObstacleSpawnerBase.generated.h"

class AObstacleBase;
class AGridFieldBase;
class APlayerPawnBase;

UCLASS()
class UEHOMEWORKMOD20_API AObstacleSpawnerBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AObstacleSpawnerBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AObstacleBase> ObstacleClass;

	UPROPERTY(EditDefaultsOnly)
	int MinObstacleBlocks;

	UPROPERTY(EditDefaultsOnly)
	int MaxObstacleBlocks;

	UPROPERTY(EditDefaultsOnly)
	float MinObstacleLifeTime;

	UPROPERTY(EditDefaultsOnly)
	float MaxObstacleLifeTime;

	UPROPERTY()
	AGridFieldBase* Grid;

	UPROPERTY()
	TArray<AObstacleBase*> Obstacles;

	UPROPERTY()
	APlayerPawnBase* Pawn;

	void SpawnObstacle();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void Init(AGridFieldBase* grid, APlayerPawnBase* pawn);

	void OnObstacleDestroyed(int gridElementIndex);

};
