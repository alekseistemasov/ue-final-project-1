// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGameModeBase.generated.h"

class AFoodSpawnerBase;
class AObstacleSpawnerBase;
class AGridFieldBase;

/**
 * 
 */
UCLASS()
class UEHOMEWORKMOD20_API ASnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TSubclassOf<AFoodSpawnerBase> FoodSpawnerClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AObstacleSpawnerBase> ObstacleSpawnerClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AGridFieldBase> GridClass;

	UPROPERTY(EditDefaultsOnly)
	FVector GridPosition;

	virtual void StartPlay() override;

	UFUNCTION(BlueprintNativeEvent)
	void SnakeStarted();
	void SnakeStarted_Implementation();

	UFUNCTION(BlueprintNativeEvent)
	void SnakeDestroyed();
	void SnakeDestroyed_Implementation();

protected:
	UPROPERTY()
	AGridFieldBase* Grid;

	UPROPERTY()
	AFoodSpawnerBase* FoodSpawner;

	UPROPERTY()
	AObstacleSpawnerBase* ObstacleSpawner;

	

	


	
};
