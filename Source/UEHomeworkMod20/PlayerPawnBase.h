// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeActorBase;
class AGridFieldBase;
class ASnakeGameModeBase;

UCLASS()
class UEHOMEWORKMOD20_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* Camera;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeActorBase> SnakeActorClass;

	UPROPERTY()
	AGridFieldBase* Grid;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadWrite)
	ASnakeActorBase* SnakeActor;

	UPROPERTY()
	ASnakeGameModeBase* GameMode;

	void CreateSnakeActor(AGridFieldBase* grid);

	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
	void HandleVerticalInput(float value);

	UFUNCTION()
	void HandleHorizontalInput(float value);

	UFUNCTION()
	void HandleSpacePressed();

	UFUNCTION(BlueprintCallable)
	int GetScore();

	UFUNCTION(BlueprintCallable)
	float GetLifeTimePercent();

	void Init(AGridFieldBase* grid, ASnakeGameModeBase* gameMode);

	int GetSnakeHeadGridIndex();

	void OnSnakeDestroyed();
		
	

};
