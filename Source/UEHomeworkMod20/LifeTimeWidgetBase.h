// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "LifeTimeWidgetBase.generated.h"

class AFoodBase;
/**
 * 
 */
UCLASS()
class UEHOMEWORKMOD20_API ULifeTimeWidgetBase : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintReadOnly)
	AFoodBase* Food;
};
