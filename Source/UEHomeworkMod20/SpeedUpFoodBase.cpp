// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedUpFoodBase.h"
#include "SnakeActorBase.h"
#include "FoodSpawnerBase.h"

// Sets default values
ASpeedUpFoodBase::ASpeedUpFoodBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	LifeTimer = 10;
	Points = 10;
	TimeToAdd = 5;

}

// Called when the game starts or when spawned
void ASpeedUpFoodBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpeedUpFoodBase::Tick(float DeltaTime)
{
	AFoodBase::Tick(DeltaTime);
	

}

void ASpeedUpFoodBase::Interact(AActor* interactor, bool bIsHead)
{
	ASnakeActorBase* snake = Cast<ASnakeActorBase>(interactor);
	if (IsValid(snake)) {
		float newSpeed = snake->GetActorTickInterval() - SpeedToAdd;
		if (newSpeed <= 0) return;
		snake->SetActorTickInterval(newSpeed);
		AFoodBase::Interact(interactor, bIsHead);
	}
}

