// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FoodBase.h"
#include "SlowDownFoodBase.generated.h"

class AFoodSpawnerBase;

UCLASS()
class UEHOMEWORKMOD20_API ASlowDownFoodBase : public AFoodBase
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASlowDownFoodBase();

	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly)
	float SpeedToReduce;

	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* interactor, bool bIsHead) override;

};
