// Fill out your copyright notice in the Description page of Project Settings.


#include "ObstacleBase.h"
#include "ObstacleSpawnerBase.h"
#include "SnakeActorBase.h"

// Sets default values
AObstacleBase::AObstacleBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.SetTickFunctionEnable(false);
	StartLifeTime = 0;
	LifeTimer = 0;

}

// Called when the game starts or when spawned
void AObstacleBase::BeginPlay()
{
	Super::BeginPlay();
	
	
}

// Called every frame
void AObstacleBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	LifeTimer -= DeltaTime;
	if (LifeTimer <= 0) {
		if (IsValid(Spawner)) Spawner->OnObstacleDestroyed(GridElementIndex);
		Destroy();
	}

}

float AObstacleBase::GetLifeTimePercent()
{
	return LifeTimer / StartLifeTime;
}

void AObstacleBase::Init(float LifeTime)
{
	this->StartLifeTime = LifeTime;
	LifeTimer = LifeTime;
	PrimaryActorTick.SetTickFunctionEnable(true);
}

void AObstacleBase::Interact(AActor* interactor, bool bIsHead)
{
	ASnakeActorBase* snake = Cast<ASnakeActorBase>(interactor);
	if (IsValid(snake)) {
		if (!snake->GetObstacleFreeStatus()) snake->DestroySnake();
	}
}

