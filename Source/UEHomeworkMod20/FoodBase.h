// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "FoodBase.generated.h"

class AFoodSpawnerBase;
class UWidgetComponent;
class ULifeTimeWidgetBase;
class UStaticMeshComponent;


UCLASS()
class UEHOMEWORKMOD20_API AFoodBase : public AActor, public IInteractable 
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFoodBase();

	UPROPERTY()
	AFoodSpawnerBase* Spawner;

	UPROPERTY()
	int GridElementIndex;

	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly)
	int Points;

	UPROPERTY()
	float LifeTimer;

	
	float StartLifeTime;

	UPROPERTY(EditDefaultsOnly)
	float TimeToAdd;

	UPROPERTY(VisibleAnywhere)
	UWidgetComponent* LifeTimeWidget;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ULifeTimeWidgetBase> WidgetClass;

	UPROPERTY(EditDefaultsOnly)
	float MinLifeTime;

	UPROPERTY(EditDefaultsOnly)
	float MaxLifeTime;

	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	float GetLifeTimePercent();

	virtual void Interact(AActor* interactor, bool bIsHead) override;

};
