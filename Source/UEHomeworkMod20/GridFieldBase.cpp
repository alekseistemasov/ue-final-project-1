// Fill out your copyright notice in the Description page of Project Settings.


#include "GridFieldBase.h"
#include "SnakeActorBase.h"
#include "WallBase.h"

// Sets default values
AGridFieldBase::AGridFieldBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	GridSize = 50;
	ElementSize = 100;

}

// Called when the game starts or when spawned
void AGridFieldBase::BeginPlay()
{
	Super::BeginPlay();
	CreateGrid();
	CreateWall();
	
}

void AGridFieldBase::CreateGrid()
{
	FVector newLocation(GetActorLocation());
	for (int i = 0; i < GridSize; i++) {
		for (int j = 0; j < GridSize; j++) {
			FGridElement element = FGridElement();
			int newIndex = GridElements.Add(element);
			GridElements[newIndex].Position = newLocation;
			GridElements[newIndex].bIsEmpty = true;
			GridElements[newIndex].Index = newIndex;
			newLocation.Y += ElementSize;
		}
		newLocation.X += ElementSize;
		newLocation.Y = GetActorLocation().Y;
	}
}

void AGridFieldBase::CreateWall()
{
	for (int i = 0; i < GridElements.Num(); i++) {
		if (i % GridSize == 0 ||
			i + GridSize > GridElements.Num() - 1 ||
			(i + 1) % GridSize == 0 ||
			i - GridSize < 0) {
			AWallBase* wallBlock = GetWorld()->SpawnActor<AWallBase>(WallClass);
			FGridElement gridElement = TakeOverByIndex(i, i);
			wallBlock->SetActorLocation(gridElement.Position);
		}

		
	}
}

FGridElement AGridFieldBase::GetElement(int lastElementIndex, EMovementDirection direction)
{
	int newIndex = 0;
	switch (direction) {
	case EMovementDirection::LEFT: {
		if (lastElementIndex % GridSize == 0) return FGridElement();
		newIndex = lastElementIndex - 1;
		break;
	}
	case EMovementDirection::UP: {
		if (lastElementIndex + GridSize > GridElements.Num() - 1) return FGridElement();
		newIndex = lastElementIndex + GridSize;
		break;
	}
	case EMovementDirection::RIGHT: {
		if ((lastElementIndex + 1) % GridSize == 0) return FGridElement();
		newIndex = lastElementIndex + 1;
		break;
	}
	case EMovementDirection::DOWN: {
		if (lastElementIndex - GridSize < 0) return FGridElement();
		newIndex = lastElementIndex - GridSize;
		break;
	}
	}

	return GridElements[newIndex];
	
}

// Called every frame
void AGridFieldBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FGridElement AGridFieldBase::TakeOver(int lastElementIndex, EMovementDirection direction, bool bReleaseLastIndex)
{
	if (bReleaseLastIndex) GridElements[lastElementIndex].bIsEmpty = true;
	FGridElement element = GetElement(lastElementIndex, direction);
	if (element.Position == FVector::Zero()) return FGridElement();
    GridElements[element.Index].bIsEmpty = false;
	return GridElements[element.Index];

}

FGridElement AGridFieldBase::TakeOverByIndex(int lastElementIndex, int currentIndex)
{
	GridElements[lastElementIndex].bIsEmpty = true;
	GridElements[currentIndex].bIsEmpty = false;
	return GridElements[currentIndex];
}

FGridElement AGridFieldBase::TakeOverMiddle()
{
	int index = ((GridSize * GridSize) / 2) + (GridSize / 2);
	GridElements[index].bIsEmpty = false;
	return GridElements[index];
}

FGridElement AGridFieldBase::TakeOverRandom()
{
	while (true) {
		int random = FMath::RandRange(0, GridElements.Num() - 1);
		if (GridElements[random].bIsEmpty) {
			GridElements[random].bIsEmpty = false;
			return GridElements[random];
		}
	}
}

void AGridFieldBase::ReleaseByIndex(int index)
{
	GridElements[index].bIsEmpty = true;
}

FGridElement AGridFieldBase::GetInfo(int lastElementIndex, EMovementDirection direction)
{
	return GetElement(lastElementIndex, direction);
}

