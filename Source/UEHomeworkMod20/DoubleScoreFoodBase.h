// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FoodBase.h"
#include "DoubleScoreFoodBase.generated.h"

/**
 * 
 */
UCLASS()
class UEHOMEWORKMOD20_API ADoubleScoreFoodBase : public AFoodBase
{
	GENERATED_BODY()

public:
	virtual void Interact(AActor* interactor, bool bIsHead) override;

protected:
	UPROPERTY(EditDefaultsOnly)
	float DoubleScoreTime;
	
};
