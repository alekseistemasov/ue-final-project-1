// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "ObstacleBase.generated.h"

class AObstacleSpawnerBase;

UCLASS()
class UEHOMEWORKMOD20_API AObstacleBase : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AObstacleBase();

	UPROPERTY()
	AObstacleSpawnerBase* Spawner;

	UPROPERTY()
	int GridElementIndex;

	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY()
	float LifeTimer;

	UPROPERTY()
	float StartLifeTime;

	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	float GetLifeTimePercent();

	void Init(float LifeTime);

	virtual void Interact(AActor* interactor, bool bIsHead) override;

};
