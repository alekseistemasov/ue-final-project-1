// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FoodSpawnerBase.generated.h"

class AFoodBase;
class ASpeedUpFoodBase;
class ASlowDownFoodBase;
class AObstacleFreeFoodBase;
class ADoubleScoreFoodBase;
class AGridFieldBase;


UCLASS()
class UEHOMEWORKMOD20_API AFoodSpawnerBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFoodSpawnerBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFoodBase> FoodClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASpeedUpFoodBase> SpeedUpFoodClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASlowDownFoodBase> SlowDownFoodClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AObstacleFreeFoodBase> ObstacleFreeFoodClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ADoubleScoreFoodBase> DoubleScoreFoodClass;

	

	void SpawnFood();

	UPROPERTY()
	AGridFieldBase* Grid;

	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void OnFoodDestroyed(int gridElementIndex);

	void Init(AGridFieldBase* grid);
	

};
